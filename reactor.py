import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation

def plot(port = 5566):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    print "Listening in to the broadcast..."
    socket.connect ("tcp://10.145.206.5:%s" % port)
    topicfilter = "42"
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    temp_list = []
    temp_high = 0
    temp_low = 50 
    n = 0

    for update_nbr in range (30000):
        string = socket.recv()

        temp2 = string.split()[1:]

        temp = float(temp2[0])
        print 'temp:', temp

        if temp >= temp_high:
            temp_high = temp
        if temp <= temp_low:
            temp_low = temp

        print temp_high
        print temp_low

        temp_list.append(temp)
        temp_text = temp_list[-1]
        temp_show = temp_list
        if len(temp_list) > 21:
            temp_show = temp_list[-20: -1]
        if len(temp_list) > 51:
            temp_show = temp_list[-50: -1]

        n += 1
        if n > 9:
            plt.close()
            plt.plot(temp_show, '-ro')
            plt.draw()
            plt.savefig('/Users/shaoqing/Google Drive/UTAustinCourses/ME397Prog/project/mysite/tempSensor/static/tempSensor/temp.png')
            n = 0

if __name__ == "__main__":
    plot(5566);





