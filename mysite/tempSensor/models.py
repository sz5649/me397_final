from django.db import models

class Temperature(models.Model):
    temp_C = models.FloatField(default = -273)
    pub_date = models.DateTimeField('date published', auto_now_add=True, blank=True)
    count = models.IntegerField(default=0)
    def __unicode__(self):              # __unicode__ on Python 2
        return str(self.temp_C)

# class Choice(models.Model):
#     question = models.ForeignKey(Question)
#     choice_text = models.CharField(max_length=200)
#     votes = models.IntegerField(default=0)
